import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Task } from 'src/app/Model/task';
import { Observable, of } from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

 

const httpOptions = {
  headers: new HttpHeaders()
       .append("Content-Type", "application/json; charset=UTF-8")
       .append('Access-Control-Allow-Headers', 'Content-Type')
       .append('Access-Control-Allow-Methods', '*')
       .append('Access-Control-Allow-Origin', '*')
       .set("Accept", 'application/json; charset=UTF-8')
};
 const apiUrl: string ="http://localhost:53342/api/"; 
//const apiUrl:string ="http://localhost/TaskManagerService/api/";


@Injectable({
  providedIn: 'root'
})

export class TaskApiService {

  constructor(private httpClient:HttpClient) { }
 
  public errMessage:string="";

  public RootUrl :string =apiUrl;

  private handleError<T> (result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.log(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  GetAllTasks (): Observable<Task[]> {

    return this.httpClient.get<Task[]>(apiUrl +"tasks")
      .pipe(
        tap(() => console.log('Get All Tasks')),
        catchError(this.handleError([]))
      );
  }
  
 

   GetTask(taskId:number)
  {
    return  this.httpClient.get<Task>(apiUrl+"tasks/"+ taskId);
  }
 

  
  PostTask (task:Task) :Observable<any>
   {
    const url = `${apiUrl}`+"tasks";
    return this.httpClient.post<Task>(url, task, httpOptions)
    .pipe(  
     catchError(this.handleError<Task>()));
          
  }
  
  PutTask (taskId:number, task:Task):Observable<Task> 
   {
    const url = `${apiUrl}`+"tasks"+`/${taskId}`;
    
    return this.httpClient.put<Task>(url, task, httpOptions).pipe(
      tap(_ => console.log(`updated task taskId=${taskId}`)),
      catchError(this.handleError<Task>())
    );
  }
  




}
