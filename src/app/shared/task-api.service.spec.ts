import { TestBed } from '@angular/core/testing';
import {HttpClientModule,HttpClient } from '@angular/common/http';
import { TaskApiService } from './task-api.service';
import { Task } from '../Model/task';
import {HttpTestingController, HttpClientTestingModule} from '@angular/common/http/testing';



describe('TaskAPI service', () => {
  let service: TaskApiService;
  let httpMock: HttpTestingController;

  const testTasks: Task[] = [{
    taskId: 1,
    taskName: 'First Cry',
    parentTaskName: 'No Parent task',
    priority: 17,
    startDate : new Date(2019,12,1),
    endDate : new Date(2019,12,10),
    status : 'OPEN',
    parentTaskId : 0,
    isTaskEnded : false
    }, 
    {
      taskId: 1,
      taskName: 'Second Cry',
      parentTaskName: 'No Parent task',
      priority: 17,
      startDate : new Date(2019,12,1),
      endDate : new Date(2019,12,10),
      status : 'OPEN',
      parentTaskId : 0,
      isTaskEnded : false
      }
    ];

  
    beforeEach(() => {
      TestBed.configureTestingModule({
          imports: [HttpClientModule,  HttpClientTestingModule],
          providers: [TaskApiService]
          
      });
      service = TestBed.get(TaskApiService);
      httpMock =TestBed.get(HttpTestingController);
    });
  
  
  
    it('should be created', () => {
        expect(service).toBeTruthy();
      });

      it('should  get the Tasks details wihtout any errors' , ()=>{
        expect(service.errMessage).toEqual(''); 
       });

      it('be able to retrieve Tasks from the API bia GET', () => {
        service.GetAllTasks().subscribe(myTasks => {
            expect(myTasks.length).toBe(2);
            expect(myTasks).toEqual(testTasks);
        });       
   

});

describe('API service Cleaning ', ()=>{
  afterEach(() => {
    httpMock.verify();
});

})  


});