import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/Model/task';
import { TaskApiService } from 'src/app/shared/task-api.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { BsDatepickerConfig, DateFormatter } from 'ngx-bootstrap/datepicker'
import { ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { CanComponentDeactivate } from 'src/app/shared/CanDeactivateGuard';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements  OnInit, CanComponentDeactivate {
  @ViewChild('form', { static: true }) 
  public addTaskform:NgForm;
  public datepickerConfig: BsDatepickerConfig;
  public currentTask: Task=new Task();
  public tasks :Task[];
  public error:any={isError:false, ErrorMessage:"" };
  public isValidDate:any;

  constructor(private _route:Router, private _taskService: TaskApiService) {
  
    this.datepickerConfig = new BsDatepickerConfig();
    this.datepickerConfig.containerClass='theme-dark-blue';
    this.datepickerConfig.showWeekNumbers=false;
    this.datepickerConfig.dateInputFormat="DD/MM/YYYY";
    
    this.datepickerConfig.minDate= new Date(2018,12,1);
    this.datepickerConfig.maxDate= new Date(2020,12,31);

   }

   canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.addTaskform.dirty)
      {
        return confirm('You have unsaved information! Do you want to leave the current screen?');
    }
    return true;
  }

  ngOnInit() {
     this.resetForm(); 
     
     this._taskService.GetAllTasks()
     .subscribe(data =>this.tasks=data);
     }

     Clear(): void{
       this.resetForm();
       
     }

  resetForm(form?:NgForm) 
  {
    if(form !=null)
    {
     form.resetForm();
         }
       this.currentTask =new Task();
       this.currentTask.taskId=0;
       this.currentTask.taskName='';
       this.currentTask.parentTaskId=0;
       this.currentTask.parentTaskName='';
       this.currentTask.priority=0;
       this.currentTask.startDate = null;
       this.currentTask.endDate = null;
       this.currentTask.status='OPEN';
       this.currentTask.isTaskEnded=false;
    }

  onsubmit(form : NgForm) : void
  {
    if(this.validate())
    {
      this.currentTask.startDate =new Date(this.currentTask.startDate.toLocaleDateString());
      this.currentTask.endDate =new Date(this.currentTask.endDate.toLocaleDateString());
       this._taskService.PostTask(this.currentTask).subscribe
       (data=>{
         this.tasks.push(this.currentTask);
         this.resetForm(form);
         this._route.navigate(['/tasks']);
        }
      );
    }
  }

  validate(): boolean
  {
      if( this.currentTask.startDate ==null)
      {
          alert( "Start date is required");
           return false;
      }
      
      if( this.currentTask.endDate ==null)
      {
          alert( "End date is required");
           return false;
      }

      if(this.currentTask.endDate <= this.currentTask.startDate)
      {
            alert('end date should be greater than start date');
            return false;
      }
      return true;
  }
}

  
