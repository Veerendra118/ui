import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Task } from 'src/app/Model/task';
import { TaskApiService } from 'src/app/shared/task-api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker'
import { ViewChild } from '@angular/core';
import { CanComponentDeactivate } from 'src/app/shared/CanDeactivateGuard';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css']
})
export class UpdateTaskComponent implements OnInit, CanComponentDeactivate {
  @ViewChild('form', { static: true }) public addTaskform:NgForm;
  public datepickerConfig: BsDatepickerConfig;
  private isFound:boolean =false;
  public currentTask: Task = new Task();
  curTaskId: number;
  public tasks: Task[] = [];

  constructor(private _taskService: TaskApiService
    , private route: ActivatedRoute, private router: Router) {

      this.datepickerConfig = new BsDatepickerConfig();
      this.datepickerConfig.containerClass='theme-dark-blue';
      this.datepickerConfig.showWeekNumbers=false;
      this.datepickerConfig.dateInputFormat="DD/MM/YYYY";
      this.datepickerConfig.minDate= new Date(2018,12,1);
      this.datepickerConfig.maxDate= new Date(2020,12,31);
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.addTaskform.invalid)
      {
        return confirm('You have unsaved information! Do you want to leave the current screen?');
    }
    return true;
  }

  ngOnInit() {
    //this.resetForm();
   
     this.route.paramMap
    .subscribe(parameter => {
      this.curTaskId = +parameter.get('taskId')
      
      this._taskService.GetAllTasks()
      .subscribe(data => {
        var  alltasks  = data;
        this.tasks =alltasks.filter( x=>x.parentTaskId !=this.curTaskId && x.taskId !=this.curTaskId );          
        for(var t of alltasks)
        {
          if(t.taskId ===this.curTaskId )
          {
            this.currentTask =t;
            //This is required without this it will not display correctly.
            this.currentTask.startDate = new Date(t.startDate);
            this.currentTask.endDate = new Date(t.endDate);
            this.isFound=true;
            break;
          }
        }
        if(!this.isFound)
        {
          
          this.router.navigate(['**']);
        }
      });
    });

    
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.currentTask = new Task();
    this.currentTask.taskId = 0;
    this.currentTask.taskName = '';
    this.currentTask.parentTaskId = 0;
    this.currentTask.parentTaskName = '';
    this.currentTask.priority = 0;
    this.currentTask.startDate = null;
    this.currentTask.endDate = null;
    this.currentTask.status = 'OPEN';
    this.currentTask.isTaskEnded = false;
  }

  onsubmit(form: NgForm): void {
    
    if(this.validate())
    {
    this._taskService.PutTask(this.currentTask.taskId, this.currentTask)
      .subscribe(data => {
          
          this.router.navigate(['/tasks']);
      }
      );
    }
  }


  validate(): boolean
  {

      if(this.currentTask.endDate <= this.currentTask.startDate)
      {
            alert('end date should be greater than start date');
             return false;
      }
      return true;

  }

}
