import { Component, OnInit } from '@angular/core';
import { TaskApiService } from 'src/app/shared/task-api.service';
import { Task } from 'src/app/Model/task';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker'


@Component({
  selector: 'app-view-tasks',
  templateUrl: './view-tasks.component.html',
  styleUrls: ['./view-tasks.component.css']
})

export class ViewTasksComponent implements OnInit {
 
  public datepickerConfig: BsDatepickerConfig;
  public tasks:Task[]=[];
  public dbTasks:Task[]=[];
 
  
  private _taskName:string;
  private _parentTaskName:string;
  private _priorityFrom:number;
  private _priorityTo:number;
  private _startDate:Date;
  private _endDate:Date;


  get startDate():Date
  {
    return this._startDate;
  }
  set startDate(value:Date)
  {
      this._startDate=value;
  }

  get endDate():Date
  {
    return this._endDate;
  }
  set endDate(value:Date)
  {
      this._endDate=value;
  }


  get taskName():string
  {
     return this._taskName;
  }
  set taskName(value:string){
      this._taskName =value;
  }

  get parentTaskName():string
  {
     return this._parentTaskName;
  }
  set parentTaskName(value:string){
      this._parentTaskName =value;
  }
  
  get priorityFrom():number{
    return  this._priorityFrom;
  }
  set priorityFrom(value:number){
     this._priorityFrom=value;
  }

  get priorityTo():number{
    return  this._priorityTo;
  }
  set priorityTo(value:number){
     this._priorityTo=value;
  }




  constructor( private _taskApiService: TaskApiService) { 

        let d =  new Date();

        this.taskName="";
        this.parentTaskName="";
        this.priorityFrom=1;
        this.priorityTo=30;
        this.startDate=new Date(d.getFullYear(),0, 1) ;
        this.endDate=new Date( d.getFullYear()+ 1,4, 1) ;
        
      this.datepickerConfig = new BsDatepickerConfig();
      this.datepickerConfig.containerClass='theme-dark-blue';
      this.datepickerConfig.showWeekNumbers=false;
      this.datepickerConfig.dateInputFormat="MM/DD/YYYY";
      this.datepickerConfig.minDate= new Date(2018,0,1);
      this.datepickerConfig.maxDate= new Date(2019,12,31);
      

      }

  ngOnInit() {
    this._taskApiService.GetAllTasks()
         .subscribe(data=>this.tasks=data);


  }


  EndOfTask(t:Task)
  {
     if(confirm('Are you sure, you want to end the task:'+ t.taskName +' ?')) 
     {
          t.isTaskEnded=true;
          t.status="CLOSED";
          this._taskApiService.PutTask(t.taskId,t).subscribe(
            (()=>{console.log('task is endded');
            this._taskApiService.GetAllTasks()
                .subscribe(data =>this.tasks=data);           
          })
          );
     }
  }
}
