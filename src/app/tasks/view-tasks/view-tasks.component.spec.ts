import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { ViewTasksComponent } from './view-tasks.component';
import { FormsModule } from '@angular/forms';
//import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/ngx-bootstrap-datepicker';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

describe('ViewTasksComponent', () => {
  let component: ViewTasksComponent;
  let fixture: ComponentFixture<ViewTasksComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ,BrowserModule,  BrowserAnimationsModule,  HttpClientModule ],
      declarations: [ ViewTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    
  });

  // it('should Create the view tasks', () => {
  //   expect(component).toBeFalsy() ;
  // });
});
