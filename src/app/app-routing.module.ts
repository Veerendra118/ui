import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTaskComponent } from 'src/app/tasks/add-task/add-task.component';
import { ViewTasksComponent } from 'src/app/tasks/view-tasks/view-tasks.component';
import { UpdateTaskComponent } from 'src/app/tasks/update-task/update-task.component';
import { PageNotFoundComponent } from 'src/app/tasks/page-not-found/page-not-found.component';
import { CanDeactivateGuard } from './shared/CanDeactivateGuard';

const routes: Routes = [
  {path:'',  redirectTo:'/tasks', pathMatch:'full'  },
  {  path:'tasks', 
     component:ViewTasksComponent
 },
  {path:'addtask', 
   component:AddTaskComponent,
   canDeactivate:[CanDeactivateGuard]
   
 },
  {path:'updatetask/:taskId',
     component:UpdateTaskComponent,
     canDeactivate:[CanDeactivateGuard]
     },
  {path:"**", component:PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents =[ViewTasksComponent,AddTaskComponent,UpdateTaskComponent,PageNotFoundComponent ]