export class Task {
    public taskId:number;
    public taskName:string;
    public parentTaskId:number;
    public parentTaskName:string;
    public priority: number;
    public startDate:Date;
    public endDate: Date;
    public status:string;
    public isTaskEnded : boolean;
}
