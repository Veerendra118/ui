import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './tasks/page-not-found/page-not-found.component';
import { TaskApiService } from 'src/app/shared/task-api.service';
import {HttpClientModule} from '@angular/common/http'
import { BsDatepickerModule} from 'ngx-bootstrap/datepicker'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import { TaskFilterPipe } from './tasks/view-tasks/TaskFilter.pipe';
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
 
@NgModule({
  declarations: [
    AppComponent,
   routingComponents,
   PageNotFoundComponent,
   TaskFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DlDateTimeDateModule,  // <--- Determines the data type of the model
    DlDateTimePickerModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot()
    
  ],
  providers: [ TaskApiService
               
              ],
  bootstrap: [AppComponent]
})
export class AppModule { }

